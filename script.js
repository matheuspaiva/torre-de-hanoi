let border = 0
let lastTower;

function addBorder(tower, towerId){
    if(border === 0){
        tower.style.border = '3px solid #ffc7de';
        lastTower = towerId;
        border ++;
        
    }else{
        border = 0;
        const retiraBorda = document.getElementById(lastTower)
        retiraBorda.style.border = 'none';
    }


}
function addTower(){
    let towers = document.getElementById('towersBox');
    let tower1 = document.createElement("div");
    tower1.id = 'towerOne';
    tower1.classList.add('tower');
    towers.appendChild(tower1);

    
    let tower2 = document.createElement("div");
    tower2.id = 'towerTwo';
    tower2.classList.add('tower');
    towers.appendChild(tower2);

    
    let tower3 = document.createElement("div");
    tower3.id = 'towerThree';
    tower3.classList.add('tower');
    towers.appendChild(tower3);
    
}

function addRing () {
    let tower1 = document.getElementById("towerOne");
    let disc4 = document.createElement("div");
    disc4.id = 'discFour';
    disc4.classList.add('disc'); 
    tower1.append(disc4);

    let disc3 = document.createElement("div");
    att = document.createAttribute("id");
    disc3.id = 'discThree';
    disc3.classList.add('disc');
    tower1.append(disc3);

    let disc2 = document.createElement("div");
    disc2.id = 'discTwo';
    disc2.classList.add('disc');
    tower1.append(disc2);
   

    let disc1 = document.createElement("div");
    disc1.id = 'discOne';
    disc1.classList.add('disc');
    tower1.append(disc1);
    
}

addTower();
addRing ();


let arr = []
let click = 0
let lastMove = []
let bt__ResetGame = document.getElementById("bt__ResetGame")

bt__ResetGame.addEventListener('click', function(e) {
    reflesh()
})

let bt__PlayAgain = document.getElementById("bt__PlayAgain")
bt__PlayAgain.addEventListener('click', function(e) {
    let towerThree = document.getElementById("towerThree")
    if(towerThree.childElementCount === 4){
        reflesh()
    }
    
})

let towerOne = document.getElementById("towerOne")
towerOne.addEventListener('click', function(e) {
    addBorder(towerOne, 'towerOne')
    captchClick(towerOne)
})

let towerTwo = document.getElementById("towerTwo")
towerTwo.addEventListener('click', function(e) {
    captchClick(towerTwo)
    addBorder(towerTwo, 'towerTwo')
})

let towerThree = document.getElementById("towerThree")
towerThree.addEventListener('click', function(e) {
    captchClick(towerThree)
    addBorder(towerThree, 'towerThree')
})

let bt__ReturnMove = document.getElementById("bt__ReturnMove")
bt__ReturnMove.addEventListener('click', function(e){
    back()
})

function captchClick (tower) {
    arr.push(tower)
    if( arr.length == 2 ) {
        move(arr[0],arr[1])
    }
}

function back () {
    if ( lastMove.length === 0 ) {
        alert ("Você não pode voltar")
    }
    let overture = lastMove[1].lastElementChild
    let end =  lastMove[0]
    end.append(overture)
    click = click-1
    document.getElementById("counter").innerText = click
    lastMove = []
 }

function move (tower1 , tower2) {
    let lastElement1 = tower1.lastElementChild
    let lastElement2 = tower2.lastElementChild
    while (  true ) {
        if ( ! lastElement1 === null ) {
            alert (' você não pode fazer isso')
        }
        else {
            break
        }
    }
    if (lastElement2  === null) {
        if( lastElement1 !== null ) {
            clickQtd()
            tower2.append(lastElement1)
        }
    }
    else if( lastElement1 !== null && lastElement1.offsetWidth < lastElement2.offsetWidth ) {
        if (lastElement1 !== null ) {
            clickQtd()
            tower2.append(lastElement1)
        }
    }
    if ( checkIsWinner ()) {
        alert ('você ganhou')
    }
    lastMove = arr
    arr = []
}

function checkIsWinner () {
   let value = towerThree.childElementCount
   if (value === 4) {
       return true
   }
   return false
}

function reflesh() {
    window.location.reload();
}

function clickQtd() {
    click += 1
    document.querySelector ("Span").textContent = `${click}`;
}